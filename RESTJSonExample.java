
/*
 * 
 * 
 * Author: Greg Bee
 * 
 * 
 * Date: 2/8/2018
 * 
 * 
 * Description: Simple REST API example with JSON return.
 * 
 * 
 * 
 * 			Created: 		2/8/2018
 * 
 * 
 * 
 */

import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class RESTJSonExample {

	public static void main(String[] args) {

		HttpGet httpGetRequest = new HttpGet(
				"https://api.themoviedb.org/3/tv/popular?api_key=706c5374bedbca5ed0285e7a698b90a8&language=en-US&page=1");

		CloseableHttpClient httpclient = HttpClients.createDefault();

		try {
			CloseableHttpResponse response = httpclient.execute(httpGetRequest);
			System.out.println(response.getStatusLine().toString());
			System.out.println(response.getEntity().toString());

			HttpEntity entity = response.getEntity();
			if (entity != null) {
				String jSONdata = EntityUtils.toString(entity);
				System.out.println(jSONdata);

				JSONParser jsonParser = new JSONParser();

				JSONObject jsonObject = null;
				try {
					jsonObject = (JSONObject) jsonParser.parse(jSONdata.toString());
				} catch (ParseException e) {
					e.printStackTrace();
				}

				System.out.println("The total_pages is: " + jsonObject.get("total_pages"));
				System.out.println("The total_results is: " + jsonObject.get("total_results"));

			}

		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			httpclient.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
